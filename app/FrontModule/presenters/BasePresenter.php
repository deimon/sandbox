<?php

namespace App\FrontModule\Presenters;

use Nette;

class BasePresenter extends Nette\Application\UI\Presenter {

    /** @var Nette\Database\Context */
    protected $database;

    public function injectDatabase(\Nette\Database\Context $database) {
        $this->database = $database;
    }

}
