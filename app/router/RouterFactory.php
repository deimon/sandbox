<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

class RouterFactory {

    use Nette\StaticClass;

    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter() {
        $router = new RouteList();

        $router[] = new Route('admin/[<locale=cs en|ru|cs>/]<presenter>[/<action>][/<id>]', array(
            'module' => 'Admin',
            'presenter' => 'Dashboard', 
            'action' => 'default',
            'id' => NULL,
        ));

        $router[] = new Route('[<locale=cs en|ru|cs>/]<presenter>[/<action>][/<id>]', array(
            'module' => 'Front',
            'presenter' => 'Homepage',
            'action' => 'default',
            'id' => NULL
        ));

//        $router[] = new Route('[<locale=ru en|ru|cs>/]<presenter>[/<sef>][/<action>][/<id>]', array(
//            'module' => 'Front',
//            'presenter' => 'Homepage', 
//            'sef' => NULL,
//            'action' => 'default',
//            'id' => NULL,
//        ));

        return $router;
    }

}
