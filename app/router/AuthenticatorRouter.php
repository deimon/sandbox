<?php

namespace App;

use Nette;

class AuthenticatorRouter extends Nette\Object implements Nette\Security\IAuthenticator {

    private $authenticators = [];
    private $userStorage;

    public function __construct(Nette\Security\IUserStorage $userStorage) {
        $this->userStorage = $userStorage;
    }

    public function add($namespace, Nette\Security\IAuthenticator $authenticator) {
        $this->authenticators[$namespace] = $authenticator;
    }

    public function authenticate(array $credentials) {
        $ns = $this->userStorage->getNamespace();
        if (!isset($this->authenticators[$ns])) {
            throw new \Exception;
        }
        return $this->authenticators[$ns]->authenticate($credentials);
    }

}
