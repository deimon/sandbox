<?php

namespace App\AdminModule\Presenters;

use Nette,
    Nette\Application\UI;

class SignPresenter extends BasePresenter {

    /** @persistent */
    public $backlink = '';

    protected function createComponentSignInForm() {
        $form = new UI\Form;
        $form->addText('username', $this->translator->translate('admin.login'))
                  ->setRequired($this->translator->translate('admin.pleaseFillInYourLoginName'));

        $form->addPassword('password', $this->translator->translate('admin.password'))
                  ->setRequired($this->translator->translate('admin.pleaseFillInYourLoginPassword'));

        $form->addSubmit('send', $this->translator->translate('admin.login'))->setAttribute("class", "sendButton");
        $form->onSuccess[] = array($this, 'signInFormSucceeded');
        return $form;
    }

    public function signInFormSucceeded($form, $values) {
        try {
            $this->getUser()->login($values->username, $values->password);
        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError($e->getMessage());
            return;
        }

        $this->restoreRequest($this->backlink);
        $this->redirect('Dashboard:');
    }

    public function actionOut() {
        $this->getUser()->logout();
        $this->flashMessage($this->translator->translate('admin.logoutWasCarriedOutSuccessfully'));
        $this->redirect('in');
    }

}
