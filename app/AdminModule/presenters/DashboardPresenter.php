<?php

namespace App\AdminModule\Presenters;

use Nette;

class DashboardPresenter extends BasePresenter {

//    private $model;

//    public function __construct(\App\AdminModule\Model\ProfilesRepository $model) {
//        $this->model = $model;
//    }

    public function renderDefault() {
        $this->template->admin = $this->admin;
    }

}
