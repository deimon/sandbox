<?php

namespace App\AdminModule\Presenters;

use Nette;

class BasePresenter extends Nette\Application\UI\Presenter {

    /** @persistent */
    public $locale;

    /** @var \Kdyby\Translation\Translator @inject */
    public $translator;

    /** @var Nette\Database\Context */
    protected $database;

    public function injectDatabase(\Nette\Database\Context $database) {
        $this->database = $database;
    }
    
    public function checkRequirements($element) {
        $this->getUser()->getStorage()->setNamespace('Admin');
        parent::checkRequirements($element);
    }

    

}
