<?php

namespace App\AdminModule\Presenters;

use Nette;

class BaseSecuredPresenter extends Nette\Application\UI\Presenter {

    /** @var Nette\Database\Context */
    protected $admin;

    protected function startup() {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            if ($this->getUser()->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('Z důvodu dlouhé neaktivity jste byl odhlášen. Prosím, přihlašte se znovu.');
            }
            $this->redirect('Sign:in', array('backlink' => $this->storeRequest()));
        }

        $this->admin = $this->database->table('admin')->get($this->getUser()->getId());
        if (!$this->admin) $this->redirect('Sign:in', array('backlink' => $this->storeRequest()));
        $this->admin->update(array('lastIP' => $_SERVER['REMOTE_ADDR'], 'lastLogin' => date('Y-m-d H:i:s')));
    }

}
